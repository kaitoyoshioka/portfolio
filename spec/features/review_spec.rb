require 'rails_helper'

RSpec.feature "Reviews", type: :feature do
  let!(:user) { create(:user, id: 1) }
  let!(:guest_user) { create(:user, id: 4, name: 'guest') }
  let!(:review) { create(:review, user_id: user.id) }
  let!(:review2) { create(:review, user_id: guest_user.id, content: "test2", store_name: "test_market2") }

  scenario "storeのshowページにはそのstoreに紐づいているreviewのみが表示される" do
    visit store_path("test_market")
    expect(page).to have_content review.content
    expect(page).to have_content user.name
    expect(page).not_to have_content review2.content
  end

  scenario "口コミを追加したらそのstoreのshowページにreviewが表示される" do
    log_in(user)
    visit store_path("test_market")
    expect(page).not_to have_content "test3"
    click_on "このお店の口コミを投稿"
    fill_in 'コメント', with: 'test3'
    click_on "投稿"
    visit store_path("test_market")
    expect(page).to have_content "test3"
  end
end
