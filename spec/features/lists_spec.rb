require 'rails_helper'

RSpec.feature "Lists", type: :feature do
  let!(:user) { create(:user, id: 1) }
  let!(:guest_user) { create(:user, id: 4, name: 'guest') }
  let!(:list) { create(:list, user_id: user.id) }
  let!(:list2) { create(:list, user_id: guest_user.id, product: "test2", store_name: "テストマーケット2") }
  let!(:list3) { create(:list, user_id: user.id, product: "肉", price: 500) }
  let!(:list4) { create(:list, user_id: user.id, product: "卵", price: 300) }

  scenario "storeのshowページにはそのstoreに紐づいているlistのみが表示される" do
    visit store_path("testスーパー")
    expect(page).to have_content list.product
    expect(page).to have_content list.price
    expect(page).to have_content list.amount
    expect(page).to have_content list.unit
    expect(page).to have_content list.amount
    expect(page).to have_content user.name
    expect(page).not_to have_content list2.product
  end

  scenario "userのshowページにはuserに紐づいているlistのみが表示される" do
    visit user_path(user)
    expect(page).to have_content list.product
    expect(page).to have_content list.price
    expect(page).to have_content list.amount
    expect(page).to have_content list.unit
    expect(page).to have_content list.amount
    expect(page).to have_content user.name
    expect(page).not_to have_content list2.product
  end

  scenario "買い物リストを追加したらlistが表示される" do
    log_in(user)
    visit store_path("testスーパー")
    expect(page).not_to have_content "サラダ"
    click_on "この店の買い物リストを追加する"
    fill_in '商品', with: 'サラダ'
    fill_in '金額', with: 100
    fill_in '数量', with: 1
    find("option[value='個入り']").select_option
    click_on "追加"
    visit store_path("testスーパー")
    expect(page).to have_content "サラダ"
  end
end
