require 'rails_helper'

RSpec.feature "Users", type: :feature do
  let!(:user) { create(:user, id: 1) }
  let!(:guest_user) { create(:user, id: 4, name: 'guest') }
  let!(:list) { create(:list, user_id: user.id, store_name: bookmark.store_name) }
  let!(:bookmark) { create(:bookmark, user_id: user.id) }
  let!(:bookmark2) { create(:bookmark, user_id: 4, store_name: "guest_store") }

  scenario "お気に入りにしている店舗名のみお気に入りページに表示されるか" do
    visit bookmarks_user_path(user)
    expect(page).to have_content bookmark.store_name
    expect(page).not_to have_content bookmark2.store_name
  end

  scenario "お気に入り登録ボタンが正しく動く" do
    log_in(user)
    visit store_path("テスト")
    click_on "お気に入りに登録"
    visit bookmarks_user_path(user)
    expect(page).to have_content "テスト"
    visit store_path("テスト")
    click_on "お気に入りから解除"
    visit bookmarks_user_path(user)
    expect(page).not_to have_content "テスト"
  end
end
