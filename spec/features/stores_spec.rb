require 'rails_helper'

RSpec.feature "Stores", type: :feature do
  let!(:user) { create(:user, id: 1) }
  let!(:guest_user) { create(:user, id: 4, name: 'guest') }
  let!(:list) { create(:list, user_id: user.id, store_name: bookmark.store_name) }
  let!(:bookmark) { create(:bookmark, user_id: user.id) }

  scenario "店舗名から店舗ページへ正しく移動するか" do
    visit bookmarks_user_path(user)
    expect(page).to have_content bookmark.store_name
    click_on bookmark.store_name
    expect(current_path).to eq store_path(bookmark.store_name)
    expect(page).to have_content bookmark.store_name
    expect(page).to have_content list.product
  end
end
