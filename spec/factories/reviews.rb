FactoryBot.define do
  factory :review do
    content { "test" }
    store_name { "test_market" }
  end
end
