FactoryBot.define do
  factory :list do
    product { "test" }
    price { 100 }
    amount { 1 }
    unit { "個" }
    store_name { "testスーパー" }
  end
end
