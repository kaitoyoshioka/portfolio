require 'rails_helper'

RSpec.describe List, type: :model do
  let(:user) { create(:user) }
  let(:list) { create(:list, user_id: user.id) }

  it "商品名、金額、数、単位、店舗名があれば有効" do
    expect(list).to be_valid
  end

  it "名前がなければ無効" do
    list.product = " "
    expect(list).to be_invalid
  end

  it "金額がなければ無効" do
    list.price = " "
    expect(list).to be_invalid
  end

  it "数がなければ無効" do
    list.amount = " "
    expect(list).to be_invalid
  end

  it "単位がなければ無効" do
    list.unit = " "
    expect(list).to be_invalid
  end

  it "店舗名がなければ無効" do
    list.store_name = " "
    expect(list).to be_invalid
  end
end
