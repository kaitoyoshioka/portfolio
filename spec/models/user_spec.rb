require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:user) }

  it "名前、メール、パスワードがあれば有効" do
    expect(user).to be_valid
  end

  it "名前がなければ無効" do
    user.name = " "
    expect(user).to be_invalid
  end

  it "userを削除するとuserが書いたlistも削除される" do
    user.save
    create(:list, user_id: user.id)
    expect { user.destroy }.to change(List, :count).by(-1)
  end

  it "userを削除するとuserが書いたreviewも削除される" do
    user.save
    create(:review, user_id: user.id)
    expect { user.destroy }.to change(Review, :count).by(-1)
  end

  describe "address" do
    it "メールアドレスがなければ無効" do
      user.email = " "
      expect(user).to be_invalid
    end

    it "正しくないメールアドレスは無効" do
      addresses = %w(user_at_foo.org example.user@foo.foo@bar_baz.com foo@bar+baz.com foo@bar..com)
      addresses.each do |address|
        expect(build(:user, email: address)).to be_invalid
      end
    end

    it "重複したメールアドレスは無効" do
      create(:user, email: "aaron@example.com")
      user.email = "aaron@example.com"
      expect(user).to be_invalid
    end

    it "メールアドレスは大文字小文字を同じ値にしているか" do
      user.email = "Foo@ExAMPle.CoM"
      user.save!
      expect(user.reload.email).to eq "foo@example.com"
    end
  end

  describe "password" do
    it "パスワードがなければ無効" do
      user.password = user.password_confirmation = "a" * 6
      expect(user).to be_valid

      user.password = user.password_confirmation = " " * 6
      expect(user).to be_invalid
    end

    it "パスワードが５桁以下は無効" do
      user.password = user.password_confirmation = "a" * 6
      expect(user).to be_valid
      user.password = user.password_confirmation = "a" * 5
      expect(user).to be_invalid
    end
  end
end
