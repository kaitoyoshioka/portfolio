require 'rails_helper'

RSpec.describe Review, type: :model do
  let(:user) { create(:user) }
  let(:review) { create(:review, user_id: user.id) }

  it "コメント、店名、ユーザーがあれば有効" do
    expect(review).to be_valid
  end

  it "コメントがなければ無効" do
    review.content = " "
    expect(review).to be_invalid
  end

  it "コメントが140字以上は無効" do
    review.content = "a" * 141
    expect(review).to be_invalid
    review.content = "a" * 140
    expect(review).to be_valid
  end

  it "店名がなければ無効" do
    review.store_name = " "
    expect(review).to be_invalid
  end

  it "ユーザーがなければ無効" do
    review.user_id = " "
    expect(review).to be_invalid
  end
end
