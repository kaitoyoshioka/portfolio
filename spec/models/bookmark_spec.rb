require 'rails_helper'

RSpec.describe Bookmark, type: :model do
  let(:user) { create(:user) }
  let(:bookmark) do
    user.bookmarks.build(
      store_name: "test",
    )
  end

  it "ユーザーid、店舗名があれば有効" do
    expect(bookmark).to be_valid
  end

  it "ユーザーidがなければ無効" do
    bookmark.user_id = " "
    expect(bookmark).to be_invalid
  end

  it "店舗名がなければ無効" do
    bookmark.store_name = " "
    expect(bookmark).to be_invalid
  end
end
