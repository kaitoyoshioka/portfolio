require 'rails_helper'

RSpec.describe "Reviews", type: :request do
  let(:user) { create(:user) }
  let!(:review) { create(:review, user_id: user.id) }

  before do
    log_in user
  end

  describe "GET /create" do
    context 'パラメータが妥当な場合' do
      it "リクエストが成功すること" do
        post reviews_path, params: { review: attributes_for(:review) }
        expect(response.status).to eq 302
      end

      it "口コミが追加されること" do
        expect do
          post reviews_path, params: { review: attributes_for(:review) }
        end.to change(Review, :count).by(1)
      end

      it "リダイレクトされること" do
        post reviews_path, params: { review: attributes_for(:review) }
        expect(response).to redirect_to store_path(review.store_name)
      end
    end

    context 'パラメータが不正な場合' do
      it 'リクエストが成功すること' do
        post reviews_path, params: { review: attributes_for(:review, content: "") }
        expect(response.status).to eq 302
      end

      it '口コミが登録されないこと' do
        expect do
          post reviews_path, params: { review: attributes_for(:review, content: "") }
        end.not_to change(Review, :count)
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'リクエストが成功すること' do
      delete review_path review
      expect(response.status).to eq 302
    end

    it 'リストが削除されること' do
      expect do
        delete review_path review
      end.to change(Review, :count).by(-1)
    end

    it '店舗ページにリダイレクトすること' do
      delete review_path review
      expect(response).to redirect_to store_path(review.store_name)
    end
  end
end
