require 'rails_helper'

RSpec.describe "Sessions", type: :request do
  let(:user) { create(:user) }

  describe 'POST #create' do
    before do
      post login_path, params: { session: {
        email: user.email,
        password: user.password,
      } }
    end

    it 'リクエストが成功すること' do
      expect(response.status).to eq 302
    end

    it 'リダイレクトすること' do
      expect(response).to redirect_to user_path user
    end
  end

  describe 'DELETE #destroy' do
    before do
      delete logout_path, params: { session: { id: user.id } }
    end

    it 'リクエストが成功すること' do
      expect(response.status).to eq 302
    end

    it 'ユーザー一覧にリダイレクトすること' do
      expect(response).to redirect_to root_path
    end
  end
end
