require 'rails_helper'

RSpec.describe "Users", type: :request do
  let(:user) { create(:user, email: "test@example.com") }
  let!(:guest_user) { create(:user, id: 4, name: 'guest') }

  describe "GET /show" do
    context 'ユーザーが存在する場合' do
      before do
        log_in user
        get user_path user
      end

      it "リクエストが成功する" do
        expect(response.status).to eq 200
      end

      it 'ユーザー名が表示されていること' do
        expect(response.body).to include 'testuser'
      end
    end

    context 'ユーザーが存在しない場合' do
      it 'guestuserとしてmypageが表示されていること' do
        get user_path guest_user
        expect(response.body).to include 'guest'
      end
    end
  end

  describe 'GET #new' do
    it 'リクエストが成功すること' do
      get new_user_path
      expect(response.status).to eq 200
    end
  end

  describe 'GET #edit' do
    before do
      log_in user
      get edit_user_path user
    end

    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end

    it 'ユーザー名が表示されていること' do
      expect(response.body).to include 'testuser'
    end

    it 'メールアドレスが表示されていること' do
      expect(response.body).to include 'test@example.com'
    end
  end

  describe 'POST #create' do
    before do
      log_in user
    end

    it 'リクエストが成功すること' do
      post users_path, params: { user: attributes_for(:user) }
      expect(response.status).to eq 302
    end

    it 'ユーザーが登録されること' do
      expect do
        post users_url, params: { user: attributes_for(:user) }
      end.to change(User, :count).by(1)
    end

    it 'リダイレクトすること' do
      post users_url, params: { user: attributes_for(:user) }
      expect(response).to redirect_to root_path
    end
  end

  describe 'PUT #update' do
    before do
      log_in user
    end

    it 'リクエストが成功すること' do
      put user_path user, params: { user: attributes_for(:user, name: 'test2') }
      expect(response.status).to eq 302
    end

    it 'ユーザー名が更新されること' do
      expect do
        put user_path user, params: { user: attributes_for(:user, name: 'test2') }
      end.to change { User.find(user.id).name }.from('testuser').to('test2')
    end

    it 'リダイレクトすること' do
      put user_url user, params: { user: attributes_for(:user, name: 'test2') }
      expect(response).to redirect_to User.last
    end
  end

  describe 'DELETE #destroy' do
    before do
      log_in user
    end

    it 'リクエストが成功すること' do
      delete user_path user
      expect(response.status).to eq 302
    end

    it 'ユーザーが削除されること' do
      expect do
        delete user_path user
      end.to change(User, :count).by(-1)
    end

    it 'ユーザー一覧にリダイレクトすること' do
      delete user_path user
      expect(response).to redirect_to root_path
    end
  end

  describe 'get #bookmark' do
    let!(:bookmark) { create(:bookmark, user_id: user.id) }
    let!(:list1) { create(:list, user_id: user.id, store_name: bookmark.store_name, product: "りんご") }
    let!(:list2) { create(:list, user_id: user.id, store_name: bookmark.store_name, product: "肉") }
    let!(:list3) { create(:list, user_id: user.id, store_name: "not_bookmark", product: "魚") }

    before do
      log_in user
    end

    it 'リクエストが成功すること' do
      get bookmarks_user_path user
      expect(response.status).to eq 200
    end

    it 'userのお気に入りしているスーパーのlistが表示される' do
      get bookmarks_user_path user
      expect(response.body).to include list1.product
      expect(response.body).to include list2.product
    end

    it 'お気に入りしていないスーパーのlistは表示されない' do
      get bookmarks_user_path user
      expect(response.body).not_to include list3.product
    end

    it '検索キーワードに含まれるlistは表示される' do
      get bookmarks_user_path user, params: { search: "りんご" }
      expect(response.body).to include list1.product
    end

    it '検索キーワードに含まれないlistは表示されない' do
      get bookmarks_user_path user, params: { search: "りんご" }
      expect(response.body).not_to include list2.product
    end
  end
end
