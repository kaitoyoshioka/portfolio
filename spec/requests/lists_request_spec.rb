require 'rails_helper'

RSpec.describe "Lists", type: :request do
  let(:user) { create(:user, id: 1) }
  let!(:list) { create(:list, user_id: user.id) }

  before do
    log_in user
  end

  describe "GET /create" do
    context 'パラメータが妥当な場合' do
      it "リクエストが成功すること" do
        post lists_path, params: { list: attributes_for(:list) }
        expect(response.status).to eq 302
      end

      it "リストが追加されること" do
        expect do
          post lists_path, params: { list: attributes_for(:list) }
        end.to change(List, :count).by(1)
      end

      it "リダイレクトされること" do
        post lists_path, params: { list: attributes_for(:list) }
        expect(response).to redirect_to store_path(list.store_name)
      end
    end

    context 'パラメータが不正な場合' do
      it 'リクエストが成功すること' do
        post lists_path, params: { list: attributes_for(:list, product: "") }
        expect(response.status).to eq 302
      end

      it 'リストが登録されないこと' do
        expect do
          post lists_path, params: { list: attributes_for(:list, product: "") }
        end.not_to change(List, :count)
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'リクエストが成功すること' do
      delete list_path list
      expect(response.status).to eq 302
    end

    it 'リストが削除されること' do
      expect do
        delete list_path list
      end.to change(List, :count).by(-1)
    end

    it 'ユーザー画面にリダイレクトすること' do
      delete list_path list
      expect(response).to redirect_to user_path(list.user_id)
    end
  end
end
