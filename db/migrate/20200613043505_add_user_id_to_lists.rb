class AddUserIdToLists < ActiveRecord::Migration[5.2]
  def change
    add_reference :lists, :user, foreign_key: true
    add_index :lists, [:user_id, :created_at]
  end
end
