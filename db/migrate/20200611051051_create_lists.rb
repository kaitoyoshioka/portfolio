class CreateLists < ActiveRecord::Migration[5.2]
  def change
    create_table :lists do |t|
      t.string :store_name
      t.string :product
      t.integer :price
      t.integer :amount
      t.text :memo

      t.timestamps
    end
  end
end
