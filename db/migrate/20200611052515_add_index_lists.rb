class AddIndexLists < ActiveRecord::Migration[5.2]
  def change
    add_index :lists, [:store_name, :product]
  end
end
