class AddUnitToLists < ActiveRecord::Migration[5.2]
  def change
    add_column :lists, :unit, :string
  end
end
