class AddStoreNameToReviews < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :store_name, :string
    add_index :reviews, :store_name
  end
end
