class CreateBookmarks < ActiveRecord::Migration[5.2]
  def change
    create_table :bookmarks do |t|
      t.references :user, foreign_key: true
      t.string :store_name

      t.timestamps
      t.index [:user_id, :store_name], unique: true
    end
  end
end
