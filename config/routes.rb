Rails.application.routes.draw do
  root 'static_pages#home'
  resources :stores, only: [:show, :index] do
    member do
      post 'bookmark' => 'bookmarks#create'
      delete 'bookmark' => 'bookmarks#destroy'
    end
  end
  resources :users do
    member do
      get :bookmarks
    end
  end
  resources :lists, only: [:create, :destroy]
  resources :reviews, only: [:create, :destroy]
  get    'signup'      =>  'users#new'
  post   'signup'      =>  'users#create'
  get    'login'       =>  'sessions#new'
  post   'login'       =>  'sessions#create'
  delete 'logout'      =>  'sessions#destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
