class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  scope :latest_order, -> { order(created_at: :desc) }
  scope :defo, -> (page) { includes([:user]).paginate(page: page) }
end
