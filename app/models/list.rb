class List < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :store_name, presence: true
  validates :product, presence: true
  validates :price, presence: true
  validates :amount, presence: true
  validates :unit, presence: true
  scope :bookmarks, -> (bookmarks) { where(store_name: [bookmarks.pluck(:store_name)]) }
  scope :search, -> (search) { where(['product LIKE ?', "%#{search}%"]).order("price asc") }
end
