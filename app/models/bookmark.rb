class Bookmark < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :store_name, presence: true
  validates :user_id, uniqueness: { scope: :store_name }
end
