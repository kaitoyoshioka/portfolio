class ReviewsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: :destroy

  def create
    review = current_user.reviews.build(review_params)
    if review.save
      flash[:success] = '口コミが追加されました'
      redirect_back(fallback_location: store_path(review.store_name))
    else
      flash[:danger] = '口コミの追加に失敗しました'
      redirect_back(fallback_location: store_path(review.store_name))
    end
  end

  def destroy
    @review.destroy
    flash[:danger] = '口コミを削除しました'
    redirect_to store_path(@review.store_name)
  end

  private

  def review_params
    params.require(:review).permit(:content, :store_name, :user_id)
  end

  def correct_user
    @review = current_user.reviews.find_by(id: params[:id])
    redirect_to root_url if @review.nil?
  end
end
