# require 'net/http'
# require 'uri'
# require 'json'
class StoresController < ApplicationController
  def index
    if params[:place]
      place = params[:place]
    elsif params[:search]
      results = Geocoder.search(params[:search])
      if results.present?
        lat = results.first.latitude
        lon = results.first.longitude
        place = "#{lat},#{lon}"
      end
    end

    url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?'
    client = HTTPClient.new
    query = {
      language: 'ja',
      location: "#{place}",
      radius: 500,
      types: 'grocery_or_supermarket',
      key: ENV['API_KEY'],
    }
    request = client.get(url, query)
    hairetu = JSON.parse(request.body)
    @super = hairetu['results']
  end

  def show
    @name = params[:id]

    nihongo = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=#{@name}"
    enc_str = URI.encode nihongo
    client = HTTPClient.new
    q = {
      key: ENV['API_KEY'],
      language: 'ja',
    }
    request = client.get(enc_str, q)
    hairetu = JSON.parse(request.body)
    @super = hairetu['results'].first
    sort = params[:sort] || "created_at DESC"
    @list = List.new
    @lists = List.where(store_name: @name).order(sort).defo(params[:page])
    @reviews = Review.where(store_name: @name).latest_order.defo(params[:page])
    @review = Review.new
  end
end
