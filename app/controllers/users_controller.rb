class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :bookmarks]
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update, :destroy]

  def show
    @list = List.new
    if params[:search]
      @lists = List.where(user_id: @user.id).search(params[:search]).defo(params[:page])
    else
      @lists = List.where(user_id: @user.id).latest_order.defo(params[:page])
    end
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:info] = '登録が完了いたしました！'
      redirect_to root_url
    else
      render :new
    end
  end

  def update
    if @user.update(user_params)
      flash[:success] = "ユーザー情報を更新しました。"
      redirect_to @user
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    flash[:success] = 'ユーザーを削除しました。'
    redirect_to root_path
  end

  def bookmarks
    @bookmarks = @user.bookmarks.all
    if params[:search]
      @lists = List.bookmarks(@bookmarks).search(params[:search]).defo(params[:page])
    else
      @lists = List.bookmarks(@bookmarks).latest_order.defo(params[:page])
    end
  end

  private

  def set_user
    @user = User.find_by(id: params[:id])
    redirect_to root_path if @user.nil?
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
