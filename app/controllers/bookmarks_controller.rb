class BookmarksController < ApplicationController
  def create
    if logged_in?
      user = current_user
    else
      user = guest_user
    end
    store_name = params[:id]
    bookmark = user.bookmarks.new(store_name: store_name)
    if bookmark.save
      redirect_to store_path(store_name)
    else
      redirect_to store_path(store_name)
    end
  end

  def destroy
    if logged_in?
      user = current_user
    else
      user = guest_user
    end
    store_name = params[:id]
    if bookmark = user.bookmarks.find_by(store_name: store_name)
      bookmark.delete
      redirect_to store_path(store_name)
    else
      redirect_to store_path(store_name)
    end
  end
end
