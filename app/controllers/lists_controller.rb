class ListsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def create
    list = current_user.lists.build(list_params)
    if list.save
      flash[:success] = 'リストが追加されました'
      redirect_back(fallback_location: store_path(list.store_name))
    else
      flash[:danger] = 'リストの追加に失敗しました'
      redirect_back(fallback_location: store_path(list.store_name))
    end
  end

  def destroy
    @list.destroy
    flash[:danger] = 'リストを削除しました'
    redirect_to user_path(@list.user_id)
  end

  private

  def list_params
    params.require(:list).permit(:product, :store_name, :price, :amount, :unit, :user_id)
  end

  def correct_user
    @list = current_user.lists.find_by(id: params[:id])
    redirect_to root_url if @list.nil?
  end
end
